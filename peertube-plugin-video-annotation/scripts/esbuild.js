const esbuild = require('esbuild')
const { externalGlobalPlugin } = require('esbuild-plugin-external-global')

for (const file of [ 'embed-client-plugin.js', 'video-edit-client-plugin.js', 'video-watch-client-plugin.js' ]) {
  esbuild.build({
    entryPoints: [ 'client/' + file ],
    bundle: true,
    minify: false,
    format: 'esm',
    outfile: 'dist/' + file,
    target: [ 'safari11' ],

    plugins: [
      externalGlobalPlugin({
        'video.js': 'window.videojs'
      })
    ]
  })
}
